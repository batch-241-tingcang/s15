console.log("Hello World!");


// Syntax of console.log: console.log(variableOrStringToBeConsoled)

//It will comment parts of the code that gets ignored by the language

/*
	There are two types of comments
	1. the singleline comment denoted by two slashes
	2. The multi-line comment denoted by slash and asterisk
*/

// [Section] Syntax and statements
	// Statements in programming are instructions that we tell the computer to perform
	// JS statements usually end with (;)
	// JS doesn't require JS but it is best practice to add one
	// Semicolons are not required in JS but we will use it to help us train to locate where statement ends.
	// A syntax in programming, it is the set of rules that we describe statements that must be constructed
		// Syntax = formula

	// All lines/blocks of code should be written in specific manner to work. This is due to how these codes where initially programmed to function in a certain manner.

//  [Section] Variable
		// Variables are used to contain data
		// Any information that is used by an application is stored in what we call the memory
		// When we create variables, certain portions of device's memory is given a name that we call variables
		
		// This makes it easier for us to associate information stored in our devices to actual "names" information

		// Declaring Variables
			// It tells our devices that a variable name is created and is ready to store data.
			
			// Syntax 
				// let/const variableName; (camelCasing)

			let myVariable = "Ada Lovelace";
			let variable;
			// This will cause an undefined variable because the declared variable does not have initial value
			console.log(variable);
			// Const keyword is used when the value of the variable won't change.
			const constVariable = "John Doe";
			// console.log() is useful for printing values of variables or certain results of code into the browser's console.
 			console.log(myVariable);

 			/*
 				Guides in writing Variables
 					1. use the let keyword followed by the variable name of your choose and use the assignment operator (=) to assign value.
 					2. Variable names should   start with lowercase character, use camelCasing for the multiple words
 					3. For constant variables, use the 'const keyword'
 						Note: If we use const keyword in declaring a variable, we cannot change the value of its variable.
 					4. Variable names should be indicative (descriptive) of the value being stored to avoid confusion
 			*/

 		// Declare and initialize
 				// Initializing variables - the instance when a variable is given its first/initial value
 				// Syntax:
 					// let/const variableName = initial value;

 			let productName =  "desktop computer"
 			console.log(productName)
 			// let productName is the declare of variables
 			// desktop computer is the initialization

 			let desktopName;

 			desktopName = "Dell"
 			console.log(desktopName)

 			// hoisted = pwede na maaccess ang code; does not follow the top to bottom convention; var is hoisted, const and let are not; kahit na mauna ang initialization than declaring, okay lang as long as diniclare mo sya as var


 		// Reassigning value
 				// Syntax: variableName = newValue
 			productName = "Personal computer";
 			console.log(productName);

 			const name = "Mica";
 			console.log(name);
 			// This reassignment will cause an error since we cannot change/reassign the initial value of constant variable.
 			// name = "Mic";
 			// console.log(name);

 			// This will cause an error on our code because the productName variable is already taken.

 			/*let productName = "Laptop"; */

 			// var vs let/const
 				// some of you may wonbder why we used let and const keyword in declaring a variable when we search online, we usually see var.

 				// var - is also used in declaring variable but var is an ecmaScript1 feature [(1997)].


 		let lastName;
 		lastName = "Mortel";
 		console.log(lastName)

 	/* 	Using var is a bad practice
 		batch = "Batch 241";

 		var batch;
 		console.log(batch); */
 		

////////////////////

// let/const  local/global scope

/*
	Scope essentially means where these variables are available for used

	let/const are block scope

	A block is a chunk of code bounded by {}.




let outerVariable = "Hello";

{
	let innerVariable = "Hello Again";
	console.log(innerVariable);
}

console.log(outerVariable);
*/

var outerVariable = "Hello";
{
	var innerVariable = "Hello Again";
}

console.log(outerVariable);
console.log(innerVariable);

// Multiple variable declarations

// let productCode = "DC017", productBrand = "Dell";
let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode);
console.log(productBrand);

// Using a variable with a reserved keyword
// const let = "Hello";
// console.log(let);


// [SECTION] Data Types
// Strings
/*
	Strings are a series of characters that create a word, a phrase, a sentence, or anything related to creating the text

	Strings in Javascript can be written using either a single quote ('') or double quote {""}

*/
let country = "Philippines";
let province = 'Metro Manila';
console.log(country);
console.log(province);

// Concatenate strings
// Multiple string values can be combined to create a single string using the "+" symbol

let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

// Declaring a string using an escape character
let message = 'John\'s employees went home early';
console.log(message);


message = "John's employees went home early";
console.log(message);

// "\n" - refers to creating a new line in between text 
let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

// Numbers
// Integer/Whole Number

let headCount = 27;
console.log(headCount);
console.log(typeof headCount);

// Decimal Number 
let grade = 98.7;
console.log(grade);
console.log(typeof grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);
console.log(typeof planetDistance);

	// In writing huge numbers, ex: 1,000,000,comma cannot be used, instead write it in exponential form or separate by _


// String
let planetDistance1 = "2e10";
console.log(planetDistance1);
console.log(typeof planetDistance1);

// Combining text and strings
console.log("John's grade last quarter is " + grade);


// Boolean 
// Boolean values are normally used to store values relating to the state of certain things
// Values can either be true or false

let isMarried = false;
let inGoodConduct = true;
console.log(typeof isMarried);
console.log(typeof inGoodConduct);

// Arrays
// Arrays are a special kind of data type that is used to store multiple values

/*
	Syntax:
		let/const arrayName - [elementA, elementB ...];
*/

// similar data types
let grades = [98, 92.1, 90.1, 94.7];
console.log(grades);

// Array is a special kind of object. 
console.log(typeof grades);

// different data type - not recommended because you cannot identify the meaning of individual values, ex: you won't know what the meaning of 32 is
let details = ["John", 32, true];
console.log(details);

// Objects
// Objects are another special kind of data type that's used to mimic real world objects/items

/*
	Syntax:
		let/const objectName = {
			propertyA: value;
			propertyB: value
		}
*/

let person = {

	firstName: "John",
	lastName: "Smith",
	age: 32,
	isMarried: false,
	contact: ["09123456789", "8123-4567"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}

}
console.log(person);

let person1 = {};
console.log(person1);

// [98, 92.1, 90.1, 94.7]
let myGrades = {
	firstGrading: 98,
	secondGrading: 92.1,
	thirdGrading: 90.1,
	fourthGrading: 94.7
}

/*
	Constant Objects and Arrays

	We can change the element of an array to a constant variable
*/

 const anime = ["one piece", "one punch man", "your lie in April"];

// arrayName[indexNumber]
anime[0] = ['kimetsu no yaiba'];
console.log(anime);

// const anime1 = ["one piece", "one punch man", "your lie in April"];
// console.log(anime1)
// anime1 = ['kimetsu no yaiba'];
// console.log(anime1);

// Null
// It is used to intentionally express the absence of a value in a variable declaration/initialization

let spouse = null;

let myNumber = 0;
let myString = "";

//Undefined
// Represent the state of a variable that has been declared but without an assigned value

let inARelationship;
console.log(inARelationship);









































	